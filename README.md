# DISCLAIMER
This repository is deprecated and functionality moved to main veloren repository

# Instalation

```
$ cargo install --force --git https://gitlab.com/juliancoffee/veloren-i18n-check \
  --branch main
```
(`--force` is needed for update to overwrite existing binary)

# Usage
Change your directory to veloren repo and run these commans <br/>
`$ veloren-i18n-check --lang <lang_code>` <br/>
`$ veloren-i18n-check --all`

