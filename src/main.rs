use std::env::args;
use std::vec::Vec;
mod i18n;

fn main() {
    let cli: Vec<String> = args().collect();

    for (i, arg) in cli.iter().enumerate() {
        match arg.as_str() {
            "--all" => i18n::test_all_localizations(),
            "--verify" => i18n::verify_all_localizations(),
            "--lang" => i18n::test_specific_localization(cli[i+1].clone()),
            _ => continue,
        }
    }
}
